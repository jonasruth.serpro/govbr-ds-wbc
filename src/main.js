import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import '@govbr-ds/core/dist/core.min.css'
import './library/lib'

const app = createApp(App)

app.use(router)

app.mount('#app')
