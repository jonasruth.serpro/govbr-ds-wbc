import { createRouter, createWebHistory } from 'vue-router'
import AvatarView from '../pages/AvatarView.vue'
import BreadcrumbView from '../pages/BreadcrumbView.vue'
import ButtonView from '../pages/ButtonView.vue'
import CardView from '../pages/CardView.vue'
import CheckboxView from '../pages/CheckboxView.vue'
import CookiebarView from '../pages/CookiebarView.vue'
import DividerView from '../pages/DividerView.vue'
import FooterView from '../pages/FooterView.vue'
import HeaderView from '../pages/HeaderView.vue'
import HomeView from '../pages/HomeView.vue'
import InputView from '../pages/InputView.vue'
import ItemView from '../pages/ItemView.vue'
import ListView from '../pages/ListView.vue'
import LoadingView from '../pages/LoadingView.vue'
import MagicButtonView from '../pages/MagicButtonView.vue'
import MenuView from '../pages/MenuView.vue'
import MessageView from '../pages/MessageView.vue'
import NotificationView from '../pages/NotificationView.vue'
import SignInView from '../pages/SignInView.vue'
import SwitchView from '../pages/SwitchView.vue'
import TabView from '../pages/TabView.vue'
import TooltipView from '../pages/TooltipView.vue'

const router = createRouter({
  history: createWebHistory('/'),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/avatar',
      name: 'avatar',
      component: AvatarView,
    },
    {
      path: '/breadcrumb',
      name: 'breadcrumb',
      component: BreadcrumbView,
    },
    {
      path: '/button',
      name: 'button',
      component: ButtonView,
    },
    {
      path: '/card',
      name: 'card',
      component: CardView,
    },
    {
      path: '/checkbox',
      name: 'checkbox',
      component: CheckboxView,
    },
    {
      path: '/cookiebar',
      name: 'cookiebar',
      component: CookiebarView,
    },
    {
      path: '/divider',
      name: 'divider',
      component: DividerView,
    },
    {
      path: '/footer',
      name: 'footer',
      component: FooterView,
    },
    {
      path: '/header',
      name: 'header',
      component: HeaderView,
    },
    {
      path: '/input',
      name: 'input',
      component: InputView,
    },
    {
      path: '/item',
      name: 'item',
      component: ItemView,
    },
    {
      path: '/list',
      name: 'list',
      component: ListView,
    },
    {
      path: '/loading',
      name: 'loading',
      component: LoadingView,
    },
    {
      path: '/magicbutton',
      name: 'magicbutton',
      component: MagicButtonView,
    },
    {
      path: '/menu',
      name: 'menu',
      component: MenuView,
    },
    {
      path: '/message',
      name: 'message',
      component: MessageView,
    },
    {
      path: '/notification',
      name: 'notification',
      component: NotificationView,
    },
    {
      path: '/signin',
      name: 'signin',
      component: SignInView,
    },
    {
      path: '/switch',
      name: 'switch',
      component: SwitchView,
    },
    {
      path: '/tab',
      name: 'tab',
      component: TabView,
    },
    {
      path: '/tooltip',
      name: 'tooltip',
      component: TooltipView,
    },
  ],
})

export default router
