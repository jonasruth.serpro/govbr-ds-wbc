import { customEventStorybookArgType, kebabiseArgs } from '../../util/Utils.js'
import BrItem from './Item.ce.vue'

const defaultSimpleText = `Um texto simples`

const defaultImageAndText = `
  <div class="row align-items-center">
    <div class="col-auto"><img class="rounded" src="https://picsum.photos/40/40" alt="imagem de exemplo 1" /></div>
    <div class="col"><span>Item 1 - uma imagem e um texto (sem elemento de formulário)</span></div>
  </div>
`

export default {
  title: 'Componentes/Item',
  component: BrItem,
  argTypes: {
    active: {
      control: 'boolean',
      defaultValue: false,
    },
    hover: {
      control: 'boolean',
      defaultValue: false,
    },
    disabled: {
      control: 'boolean',
      defaultValue: false,
    },
    selected: {
      control: 'boolean',
      defaultValue: false,
    },
    'toggle-selected': {
      ...customEventStorybookArgType,
    },
    'toggle-open': {
      ...customEventStorybookArgType,
    },
    default: {
      control: 'text',
      defaultValue: 'Texto do item',
      description: '**[OBRIGATÓRIO]** Conteúdo do item, que deve ser passado por slot.',
      table: {
        type: {
          summary: 'string | Element',
        },
        defaultValue: {
          summary: '',
        },
      },
    },
    open: {
      defaultValue: false,
    },
    tooltipText: {
      control: 'text',
    },
    tooltipPlace: {
      options: ['top', 'right', 'bottom', 'left'],
      control: 'inline-radio',
      if: { arg: 'tooltipText', truthy: true },
    },
    tooltipType: {
      options: ['info', 'success', 'warning', 'error'],
      control: 'inline-radio',
      if: { arg: 'tooltipText', truthy: true },
    },
    tooltipTimer: {
      control: 'number',
      if: { arg: 'tooltipText', truthy: true },
    },
  },
}

const TemplateDefault = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-item v-bind="args">${args.default}</br-item>`,
})

export const SimpleText = TemplateDefault.bind({})
SimpleText.args = {
  default: defaultSimpleText + '.',
  hover: true,
}

export const Disabled = TemplateDefault.bind({})
Disabled.args = {
  default: defaultSimpleText + ' desabilitado.',
  disabled: true,
}

export const ImageAndText = TemplateDefault.bind({})
ImageAndText.args = {
  default: defaultImageAndText,
  hover: true,
}

const TemplateMultipleCheckboxes = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `
  <div class="container">
    <div class="row">
      <div class="col-sm">
        <br-item hover>
          <br-checkbox id="check-01" label="Item 1">
          </br-checkbox>
        </br-item>
      </div>
      <br-divider vertical/>
      <div class="col-sm">
        <br-item hover>
          <br-checkbox id="check-02" label="Item 2">
          </br-checkbox>
        </br-item>
      </div>
    </div>
    <br-divider/>
    <div class="row">
      <div class="col-sm">
        <br-item hover>
          <br-checkbox id="check-03" label="Item 3">
          </br-checkbox>
        </br-item>
      </div>
      <br-divider vertical/>
      <div class="col-sm">
        <br-item hover onclick="
          function checkItem(){
            // console.log(document.querySelectorAll('br-item')[3])
            document.querySelectorAll('br-item')[3].addEventListener('click', function (e) {
              const checkboxItem = e.target.querySelector('br-checkbox').shadowRoot.querySelector('input').checked
              console.log(checkboxItem)
              // if (checkboxItem) {
              //   e.target.querySelector('br-checkbox').shadowRoot.querySelector('input').checked = false
              // } else {
              //   e.target.querySelector('br-checkbox').shadowRoot.querySelector('input').checked = true
              // }
            })
            // document.querySelectorAll('br-item')[3].forEach((item) => {
            //   console.log(item.querySelector('br-checkbox').shadowRoot.querySelector('input').checked)
            //   item.querySelector('br-checkbox').shadowRoot.querySelector('input').addEventListener('click', function (e) {
            //     console.log(e)
            //   })
            // })
            // document.querySelectorAll('br-checkbox')[3].addEventListener('click', function (e) {
            //   console.log(e)
            // })
            // console.log(document.querySelectorAll('br-checkbox')[3].shadowRoot.querySelector('input').checked)
            // const check = document.querySelectorAll('br-checkbox')[3].shadowRoot.querySelector('input')
            // check.addEventListener('click', function (e) {
            //   console.log(e)
            // })
            // check.checked = false
            // if (check.checked) {
            //    check.checked = true
            //  } else {
            //    check.checked = false
            //  }
          };
          checkItem()
        ">
          <br-checkbox checked id="check-04" label="Item 4">
          </br-checkbox>
        </br-item>
      </div>
    </div>
    <br-divider/>
  </div>
  `,
})

export const MultipleCheckboxes = TemplateMultipleCheckboxes.bind({})
MultipleCheckboxes.args = {}
MultipleCheckboxes.parameters = {
  controls: {
    disable: true,
  },
}

export const ComTooltip = TemplateDefault.bind({})
ComTooltip.args = {
  default: defaultSimpleText + '.',
  hover: true,
  tooltipText: 'Lorem ipsum dolor sit amet.',
}
