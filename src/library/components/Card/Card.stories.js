import { kebabiseArgs } from '../../util/Utils.js'
import BrCard from './Card.ce.vue'
import CardHeader from './CardHeader.ce.vue'

const slotContent = `
    <br-card-content slot="content">
      <img src="https://picsum.photos/id/0/500" alt="Imagem de exemplo"/>
    </br-card-content>
`

const slotContent2 = `
    <br-card-content slot="content">
      <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
        Tempore perferendis nam porro atque ex at, numquam non optio ab
        eveniet error vel ad exercitationem, earum et fugiat recusandae
        harum? Assumenda.
      </p>
      <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
        Tempore perferendis nam porro atque ex at, numquam non optio ab
        eveniet error vel ad exercitationem, earum et fugiat recusandae
        harum? Assumenda.
      </p>
    </br-card-content>
`

const slotHeader = `
    <br-card-header slot="header">
      <div class="d-flex">
        <br-avatar imgSrc="https://picsum.photos/id/823/400"/>
        <div class="ml-3">
          <div class="text-weight-semi-bold text-up-02">Maria Amorim</div>
          <div>UX Designer</div>
        </div>
        <div class="ml-auto">
          <br-button circle icon="ellipsis-v" />
        </div>
      </div>
    </br-card-header>
`

const slotHeaderDisabled = `
    <br-card-header slot="header">
      <div class="d-flex">
        <br-avatar imgSrc="https://picsum.photos/id/823/400" />
        <div class="ml-3">
          <div class="text-weight-semi-bold text-up-02">Maria Amorim</div>
          <div>UX Designer</div>
        </div>
        <div class="ml-auto">
          <br-button circle icon="ellipsis-v" disabled/>
        </div>
      </div>
    </br-card-header>
`

const slotFooter = `
    <br-card-footer slot="footer">
      <div class="d-flex">
        <div>
          <br-button label="Button" />
        </div>
        <div class="ml-auto">
          <br-button circle icon="share-alt" />
          <br-button circle icon="heart" />
        </div>
      </div>
    </br-card-footer>
`

const slotFooterDisabled = `
    <br-card-footer slot="footer">
      <div class="d-flex">
        <div>
          <br-button label="Button" disabled />
        </div>
        <div class="ml-auto">
          <br-button circle icon="share-alt" disabled />
          <br-button circle icon="heart" disabled />
        </div>
      </div>
    </br-card-footer>
`

const slotHeaderComTooltip = `
    <br-card-header slot="header">
      <div class="d-flex">
        <br-avatar imgSrc="https://picsum.photos/id/823/400" tooltip-text="Lorem ipsum dolor sit amet."/>
        <div class="ml-3">
          <div class="text-weight-semi-bold text-up-02">Maria Amorim</div>
          <div>UX Designer</div>
        </div>
        <div class="ml-auto">
          <br-button circle icon="ellipsis-v" tooltip-text="Lorem ipsum dolor sit amet."/>
        </div>
      </div>
    </br-card-header>
`

const slotFooterComTooltip = `
<br-card-footer slot="footer">
  <div class="d-flex">
    <div>
      <br-button label="Button" />
    </div>
    <div class="ml-auto">
      <br-button circle icon="share-alt" tooltip-text="Lorem ipsum dolor sit amet." />
      <br-button circle icon="heart" tooltip-text="Lorem ipsum dolor sit amet."/>
    </div>
  </div>
</br-card-footer>
`

export default {
  title: 'Componentes/Card',
  component: BrCard,
  subcomponents: {
    'br-card-header': CardHeader,
  },
  argTypes: {
    header: {
      control: 'text',
      description: '** header slot - exclusivo para títulos, subtítulos, ícones, avatares e tags.',
      table: {
        type: {
          summary: 'Element <br-card-header slot="header">',
        },
        defaultValue: {
          summary: '',
        },
      },
    },
    content: {
      control: 'text',
      description:
        '** content slot - qualquer componente é aceitável, exceto componentes relacionados à navegação, como: carrossel, paginação, abas e menu.',
      table: {
        type: {
          summary: 'Element <br-card-content slot="content">',
        },
        defaultValue: {
          summary: '',
        },
      },
    },
    footer: {
      control: 'text',
      description: '** footer slot - exclusivo para botões e links.',
      table: {
        type: {
          summary: 'Element <br-card-footer slot="footer">',
        },
        defaultValue: {
          summary: '',
        },
      },
    },
    disabled: {
      defaultValue: false,
    },
    hover: {
      defaultValue: false,
    },
    dataExpanded: {
      control: {
        type: 'select',
        options: ['on', 'off'],
      },
      defaultValue: 'on',
    },
    hFixed: {
      defaultValue: false,
    },
  },
}

const TemplateContent = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<div class="col-sm-6 col-md-4 col-lg-3">
  <br-card v-bind="args">${args.content}  </br-card>
</div>
  `,
})

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<div class="col-sm-6 col-md-4 col-lg-3">
  <br-card v-bind="args">${args.header}${args.content}${args.footer}  </br-card>
</div>
  `,
})

const TemplateHeader = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<div class="col-sm-6 col-md-4 col-lg-3">
  <br-card v-bind="args">${args.header}  </br-card>
</div>
  `,
})

const TemplateFooter = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<div class="col-sm-6 col-md-4 col-lg-3">
  <br-card v-bind="args">${args.footer}  </br-card>
</div>
  `,
})

export const Simples = TemplateContent.bind({})
Simples.parameters = { controls: { exclude: ['header'] } }
Simples.args = {
  content: slotContent,
}

export const HeaderSlot = TemplateHeader.bind({})
HeaderSlot.args = {
  header: slotHeader,
}

export const ContentSlot = TemplateContent.bind({})
ContentSlot.args = {
  content: slotContent2,
}
//ContentSlot.parameters = { controls: { exclude: ['slotTemplate'] } }

export const FooterSlot = TemplateFooter.bind({})
FooterSlot.args = {
  footer: slotFooter,
}
//FooterSlot.parameters = { controls: { exclude: ['slotTemplate'] } }

export const Completo = Template.bind({})
Completo.args = {
  header: slotHeader,
  content: slotContent2,
  footer: slotFooter,
}
//Completo.parameters = { controls: { exclude: ['slotTemplate'] } }

export const Disabled = Template.bind({})
Disabled.args = {
  disabled: true,
  header: slotHeaderDisabled,
  content: slotContent2,
  footer: slotFooterDisabled,
}
//Disabled.parameters = { controls: { exclude: ['slotTemplate'] } }

export const Hover = Template.bind({})
Hover.args = {
  hover: true,
  header: slotHeader,
  content: slotContent2,
  footer: slotFooter,
}
//Hover.parameters = { controls: { exclude: ['slotTemplate'] } }

export const ComTooltip = Template.bind({})
ComTooltip.args = {
  header: slotHeaderComTooltip,
  content: slotContent2,
  footer: slotFooterComTooltip,
}
