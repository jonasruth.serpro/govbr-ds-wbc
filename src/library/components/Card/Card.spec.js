import { shallowMount } from '@vue/test-utils'

import Card from '../Card/Card.ce.vue'
import CardHeader from '../Card/CardHeader.ce.vue'
import CardContent from '../Card/CardContent.ce.vue'
import CardFooter from '../Card/CardFooter.ce.vue'
import Avatar from '../Avatar/Avatar.ce.vue'

describe('Card', () => {
  test('Testa se a prop disable está sendo renderizada.', () => {
    const wrapper = shallowMount(Card, {
      propsData: {
        disable: true,
      },
    })

    expect(wrapper.find('.br-card').attributes('disable')).toBe('true')
  })

  test('Testa a prop dataExpanded.', () => {
    const wrapper = shallowMount(Card, {
      propsData: {
        dataExpanded: 'off',
      },
    })

    expect(wrapper.attributes('data-expanded')).toBe('off')
  })

  test('Testa a renderização do slot header.', () => {
    const wrapper = shallowMount(Card, {
      slots: {
        header: `
          <div slot="header">
            <br-card-header >
              <div class="d-flex">
                <br-avatar imgSrc="https://picsum.photos/id/823/400" />
                <div class="ml-3">
                  <p class="h5 text-primary-default mb-0">Maria Amorim</p>
                  <span>UX Designer</span>
                </div>
                <div class="ml-auto">
                  <button type="circle">
                    <span slot="button-icon">
                      <i class="fas fa-ellipsis-v" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
              </div>
            </br-card-header>
          </div>
        `,
      },
      global: {
        stubs: {
          'br-card-header': CardHeader,
          'br-avatar': Avatar,
        },
      },
    })

    expect(wrapper.findComponent(CardHeader))
    expect(wrapper.element).toMatchSnapshot()
  })

  test('Testa a renderização do slot content.', () => {
    const wrapper = shallowMount(Card, {
      slots: {
        content: `
        <div slot="content">
          <br-card-content>
            <p>
              Lorem ipsum dolor sit, amet consectetur adipisicing elit.
              Tempore perferendis nam porro atque ex at, numquam non
              optio ab eveniet error vel ad exercitationem, earum et
              fugiat recusandae harum? Assumenda.
            </p>
          </br-card-content>
        </div>
        `,
      },
      global: {
        stubs: {
          'br-card-content': CardContent,
        },
      },
    })
    expect(wrapper.findComponent(CardContent))
    expect(wrapper.element).toMatchSnapshot()
  })

  test('Testa a renderização do slot footer.', () => {
    const wrapper = shallowMount(Card, {
      slots: {
        footer: `
          <div slot="footer">
            <br-card-footer>
              <div class="d-flex">
                <div>
                  <button label="Button" />
                </div>
                <div class="ml-auto">
                  <button type="circle">
                    <span slot="button-icon">
                      <i class="fas fa-share-alt" aria-hidden="true"></i>
                    </span>
                  </button>
                  <button type="circle">
                    <span slot="button-icon">
                      <i class="fas fa-heart" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
              </div>
            </br-card-footer>
          </div>
        `,
      },
      global: {
        stubs: {
          'br-card-footer': CardFooter,
        },
      },
    })

    expect(wrapper.findComponent(CardFooter))
    expect(wrapper.element).toMatchSnapshot()
  })
})
