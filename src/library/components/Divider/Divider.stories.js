import { kebabiseArgs } from '../../util/Utils.js'
import BrDivider from './Divider.ce.vue'

export default {
  title: 'Componentes/Divider',
  component: BrDivider,
  argTypes: {
    vertical: {
      control: 'boolean',
      default: false,
    },
    size: {
      control: {
        type: 'select',
        options: ['default', 'small', 'medium', 'large'],
      },
    },
  },
}

const TemplateVertical = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<div class="row m-5">
  <div class="col sm p-3">
    <div class="d-flex">
      <p>
        Voluptate ipsa iure placeat praesentium, sint deleniti consectetur
        quisquam neque veniam possimus, hic commodi?
      </p>
      <br-divider v-bind="args" class='d-flex mx-3'></br-divider>
      <p>
        A adipisci mollitia blanditiis itaque velit laudantium voluptatum
        molestiae quasi.
      </p>
    </div>
    <br />
    <div class="bg-secondary-07 text-secondary-01 p-3">
      <div class="d-flex">
        <p>
          Voluptate ipsa iure placeat praesentium, sint deleniti consectetur
          quisquam neque veniam possimus, hic commodi?
        </p>
        <br-divider v-bind="args" class='d-flex mx-3'></br-divider>
        <p>
          A adipisci mollitia blanditiis itaque velit laudantium voluptatum
          molestiae quasi.
        </p>
      </div>
    </div>
  </div>
</div>
  `,
})

export const Vertical = TemplateVertical.bind({})
Vertical.args = {
  vertical: true,
}

Vertical.parameters = {
  controls: { exclude: ['dashed'] },
}

const TemplateHorizontal = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<div class="row m-5">
  <div class="col sm p-3">
    <div>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
      <br-divider v-bind="args"></br-divider>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
    </div>
    <br />
    <div class="bg-secondary-07 text-secondary-01 p-3">
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
      <br-divider v-bind="args"></br-divider>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
    </div>
  </div>
</div>
  `,
})

export const Horizontal = TemplateHorizontal.bind({})
Horizontal.args = {}

Horizontal.parameters = {
  controls: { exclude: ['vertical'] },
}

export const Tracejado = TemplateHorizontal.bind({})
Tracejado.args = {
  dashed: true,
}

Tracejado.parameters = {
  controls: { exclude: ['vertical'] },
}

export const Small = TemplateHorizontal.bind({})
Small.args = {
  size: 'small',
}

Small.parameters = {
  controls: { exclude: ['vertical'] },
}

export const Medium = TemplateHorizontal.bind({})
Medium.args = {
  size: 'medium',
}

Medium.parameters = {
  controls: { exclude: ['vertical'] },
}

export const Large = TemplateHorizontal.bind({})
Large.args = {
  size: 'large',
}

Large.parameters = {
  controls: { exclude: ['vertical'] },
}
