# CHANGELOG

## [1.6.1](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.6.0...v1.6.1) (01/06/2023)


### 🪲 Correções

* **message:** adiciona closable ao br-message ([a1565aa](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a1565aa54719324d9436059992c387e702caf28b))
* permite a inclusão da família do ícone no header (list-functions) e no footer (social) ([def5533](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/def55330733d979ab2f7514f2949766b5ed7177b))

## [1.6.0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.5.0...v1.6.0) (17/05/2023)


### 🔁 Refatorado

* **checkgroup:** altera labels do exemplo ([ebae145](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/ebae1456b130b3a81fc580d5820027c3543b678f))
* **checkbox:** altera nome do método de emissão do evento change ([65416e3](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/65416e3401bb02a90204c4dec89e926c62353dd7))
* **checkgroup:** cria metodos para tarefas relacionadas ao event-bus ([bceaaee](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/bceaaee950b37b5443f564e8a50db3067690c508))
* **switch:** emite event "change" baseado na mudança da variável de estado ([2338b52](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/2338b524f50756b948581b3a99e0c566d1c4fc76))
* **checkbox:** evento on-change é emitido após o evento de change ([7a12da0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/7a12da06a8035da83376f7a5b968458c04f22c17))
* **checkgroup:** inclui identação no exemplo do checkgroup ([c083fbc](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/c083fbc5a91cc30ed964e8961eb57ca730512e22))
* **checkgroup:** inclui método relacionado ao checkgroup no mixin ([f9daff5](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/f9daff51b3f9910ec7d9237d12f088efbf6b0d0a))
* **checkgroup:** registra ação direta do usuário no mixin ([66ca17d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/66ca17d5fcc250a251f167498b691b1056c5547f))
* **switch:** renomeia bind do atributo value ([5a7d47c](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/5a7d47c0609d5446f0be238c885385f167f3f928))
* **checkgroup:** renomeia metodo para armazenamento dos filhos ([961af2c](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/961af2ca8a14da1f8466dbaed2918a11a9658a62))
* **switch:** reorganiza o código e inclui comentários ([ae9a19a](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/ae9a19a7c73a0dac180f39a3810622e9bd3d52a7))
* **checkbox:** reorganiza o código ([50ddd1d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/50ddd1d7232e2a6473339adeb6167bef2eb25025))
* **checkbox:** reorganiza os métodos em ordem alfabética ([7677955](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/76779552c2e9da696c6a05bedb2027bf128de085))
* **checkgroup:** repassa o tratamento da mudança de estado do checkbox pai para o minxin ([6e370fa](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/6e370fa1e3e6aa6b4000801e5ca0572e5b0ee690))
* **checkbox:** simplifica a escrita da lista de classes ([ae2acd0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/ae2acd0356697e2131e6b16b2c5dda5618177688))
* **checkgroup:** transfere a chamada dos handlers para o mixin ([a88a6ab](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a88a6ab60806a5d68577e89ea468d0d9996cb784))
* **switch:** trata atribuição de classes por meio de variável computada ([39dd56f](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/39dd56f3807dc1b98dc5cb4f41691c965d60587e))


### ✨ Novidades

* **checkgroup:** adapta o comportamento para o two-way data binding ([e2e96d9](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e2e96d9e6389f8360855bcce326ab2f299db4351))
* **checkgroup:** altera etapa do ciclo de vida para registro no event-bus ([3de0b69](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3de0b6934788947fcb1c21db96018fd6c7392d23))
* **checkgroup:** atualiza exemplo no storybook ([79b03b4](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/79b03b42544d963e00ce0c594e83ed3f7f710349))
* atualiza o @govbr-ds/core para 3.3.1 ([70da549](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/70da5498704105ce920f54d84b24a068d630499d))
* **signin:** atualiza o sign-in para ser utilizado como elemento âncora ([e9dc252](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e9dc252944529657aeaf030c5342ae553626ec8c)), closes [#314](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/314)
* **checkbox:** controla o estado por two-way bind ([0f75c93](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/0f75c93fa475995a39ab1d77f30bbb9d28671e13))
* **tooltip:** cria mixin tooltip e ajusta componentes ([fa13a4c](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/fa13a4cd5115db3a20c312ef819d34b77c539e4a))
* **checkbox:** emite o evento 'on-change' quando o estado checked é alterado ([7d95576](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/7d9557647e7193843f63bd79e93e69da1b7287fc))
* **cookiebar:** implementa novo componente ([1cd0430](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/1cd04303a75a8488c07220e46d153c0acccbf057))
* **checkbox:** implementa o two-way data binding de forma explicita ([c1d5980](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/c1d5980c7177b77b6a6823e56a9ac4b3a27359b8))
* **switch:** implementa o two-way data binding de forma explicita ([a61f25e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a61f25e1a3d9adc4ffc7e30bcc21f94beef91ac6))
* **checkgroup:** implementa propagação do estado do pai para o filho ([87826e3](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/87826e3cdbd024161bd6e5aedacdee980bd6d3e5))
* **checkgroup:** implementa propragação do estado dos filhos para o pai ([c19115f](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/c19115f9371cd8d7d10829cc293be7d13367bc11))
* **checkbox:** inclui comentários de código ([9bd3622](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/9bd3622de96aac1d0c81121eaf4ff16e2ba6c082))
* **checkgroup:** inclui event-bus ([a80902b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a80902be60ca2645a0b42a0ae6136bab356df038))
* **checkgroup:** inclui exemplo para o comportamento checkgroup ([3b6e690](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3b6e6908b53e95c1922864a14ec44211c9bbbfcf))
* **switch:** inclui o comportamento checkgroup ([1037c5a](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/1037c5a4751a1d63f33a769d8153ddfdcc95370b))
* **switch:** inclui prop name ([b6c16ad](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b6c16ad31cf5acd93460af28273da5083604df4b))
* **checkgroup:** inclui propriedades para declarar o comportamento checkgroup ([667e2db](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/667e2dbfd395a363bee0e0342404fcbfb13221a7))
* **switch:** inclui teste para a emissão do evento change ([34129e0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/34129e0276663f4d52dec76b4f51484983915a5e))
* **switch:** incrementa lista de eventos no storybook ([278094f](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/278094fa3d21d707c2d33523d45245f287c29027))
* **checkgroup:** merge com a branch main ([f768c28](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/f768c2812f8e72ae8761168c44fa0309d5cb7aca))
* **checkgroup:** merge com a main ([ce410ff](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/ce410ff92d0c95ee6778d23d0da1e958510bf677))
* **switch:** merge com a main ([5691389](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/5691389c04da7008c8b1d4ac51d5463d443c6704))
* **switch:** merge com o checkgroup ([50585ed](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/50585edfe106b72669dcd33d478434b5cbb118c7))
* **checkgroup:** registra os filhos de cada pai ([7606b3f](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/7606b3f7792df736ba5ba0d080129a6737f72819))
* **checkgroup:** remove antigo comportamento checkgroup ([46b67de](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/46b67def52237a7f83e516e818a71a080d7906b6))
* **checkgroup:** repassa o tratamento da mudança de estado do checkbox filho para o mixin ([187a0b8](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/187a0b89bc34c20c6c4f5e0b2b6ba3fd3e039334))
* **switch:** trata a mudança de estado ([fe4c7cc](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/fe4c7cc8738c614437b4b24ea29d6acb5ef716ab))
* **switch:** trata a propagação de atributos desconsiderando o id ([6dfdf9b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/6dfdf9bcd86e9043c149112a0d9b799ad3de4c2a))
* **checkgroup:** trata encademento do click vindo de um checkbox pai ([4e96449](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/4e964499309d77bcd66368bb39ed9b4d885d5214))
* **switch:** trata o envio de um evento customizado quando o checkbox muda de valor ([961ce60](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/961ce609e58afc5a1c0df6fe0861d33080d366c9))
* **checkgroup:** trata o estado indeterminado pela propriedade indeterminate do checkbox ([5a02962](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/5a02962594140214be947fd241b478f18fff03ea))
* **checkbox:** trata o id de forma manual ou automatica ([3332f0e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3332f0e1640920de50ee7dd514531f06f7191815))
* **switch:** trata o id de forma manual ou automatica ([1f3cc66](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/1f3cc66803307052e39afbaad4dc1a50693339ea))
* **switch:** trata o valor do atributo value do checkbox ([b3d0b4b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b3d0b4b346497c1d6bcd630a0d5bf350ba413422))


### 🪲 Correções

* **icon:**  permite passar o nome com icon de forma simples ou composta (modo de compatibilidade) ([d1de82e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d1de82e9b7f621df33567c97b584045bf658b3ce)), closes [#317](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/317) [#317](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/317)
* **menu:** ajusta menu (rodapé - mídias sociais) possibilitando o uso de outras famílias de ícones ([9b4773d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/9b4773dbd0b57f13edf24916889f43353d88a5b7)), closes [#316](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/316)
* **footer:** ajusta menu social do footer permitindo a inclusão de ícones ([5f4c049](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/5f4c049f17923fa42a2cd1880cb39515b9e7e61f)), closes [#317](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/317)
* **checkbox:** altera momento de emissão do evento change ([7f71842](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/7f71842ed5482543cef7f5cca9e3564a5ab3193f))
* **switch:** altera momento de emissão do evento change ([32d45a7](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/32d45a71567b105df09b19e45399e4cc66c24d20))
* **switch:** altera nome do custom event para change e o declara no emits ([bac4adb](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/bac4adbf783494155efc7e3a5b589d2ef9275533))
* **checkgroup:** corrige a marcação de estado de checkbox que são pai e filho ao mesmo tempo ([923ce31](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/923ce31396d8c2e880f1fde3bab57573436c6929))
* **checkgroup:** corrige acesso ao event-bus pelo storybook ([582e22d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/582e22d50dd1e4395abddcdd1f97e8b0a7db743d))
* **checkgroup:** corrige comportamento do click em um checkbox indeterminado ([3010770](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/30107708783be8676ce673f7ea2b41028f4b499c))
* **header:** corrige comportamento do modo sticky ([3e612d0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3e612d079e584b45a50ad32138a26b8a9ad8f78a)), closes [#321](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/321)
* **switch:** corrige passagem da label por slot ([a836142](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a83614233d5c014158c42be71365563bb1a5beb7))
* **checkgroup:** corrige tratamento do estado inicial indeterminate ([55409fd](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/55409fd7e9075c268c9dd6f694295fcddc16336b))
* **checkgroup:** declara emissão de evento pra two-way data binding ([11b98d1](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/11b98d180ed2a358710780157ca05ea5624b9cca))
* **checkgroup:** declara o event-bus em um novo arquivo ([859a98e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/859a98e1b44e06b1bef0ca37dbfc32ae9956e05c))
* **switch:** inclui descrição do evento change no storybook ([088cbfc](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/088cbfc44f7a36532c3bddb4651b017beabe2e31))
* **message:** refatora o código do componente br-message ([f402ccf](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/f402ccfa534d64855976fe873635c06bb9ca49b8)), closes [#319](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/319)
* **signin:** remove prop iconic ([f3e6caf](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/f3e6caf8888da418b2e396275021090b89fa3bd0)), closes [#280](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/280) [#279](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/279) [#277](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/277)
* **checkgroup:** resolve conflito de merge com a main ([10b6ee0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/10b6ee092a0246176761c8b57b102ca2f60f8865))
* **checkbox:** retira chamada de código inexistente ([34251a3](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/34251a382a40de5a2106b2910a922a4e3e76d862))
* **checkgroup:** retira identações do exemplo do checkgroup ([e7842c7](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e7842c7930b706cf3c9a17290940ff329a411941))
* **switch:** retira tratamento do id ([107d37b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/107d37b40b62c7e0dd2e8fb5c48eaee1b3ad9e5d))
* **checkbox:** trata o bouble do evento click ([1e6383e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/1e6383e1a335a302d8a7eca4fae038175f705b6f))
* **checkbox:** trata o uso do atributo value ([0e0361a](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/0e0361ae58eaf839548a1b017022076ef7f55466))
* **checkbox:** troca nome do evento emitido de on-change para change ([e6476c9](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e6476c9144c11b5d00dfb5c12f47bd5cb915f378))
* **switch:** troca slot desnecessário por span ([dc25f7c](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/dc25f7cc716cbc83235a4e453ea42cfe12226186))


### 📚 Documentação

* **checkbox:** altera nome do evento enviado na mudança de estado ([fcfa1d4](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/fcfa1d46ed1d684fc25353b179aedf17beb50ee7))
* **switch:** altera nome do evento enviado na mudança de estado ([3fb5791](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3fb5791fa400a4f91b33191c3716161e82aa18aa))
* **switch:** atualiza a documentação de código ([0eae392](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/0eae3923a1a7cc413856833d5259c811501fc880))
* atualiza o readme do projeto com instruções atualizadas ([280a778](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/280a778a08c4e59e4df0f3dbe4543687f51e8e90))
* atualiza url da wiki ([c3923d7](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/c3923d7aead98546648e1e1bc2d4e50ca89c9bff))
* **tooltip:** cria documentacao sobre utilitarios javascript no storybook ([b004aba](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b004ababf6d494a75db2ceab0803357bb2fc3647)), closes [#318](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/318)

## [1.5.0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.4.1...v1.5.0) (02/03/2023)


### :memo: Documentação

* documentação para props json e eventos ([19886a6](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/19886a6c71d8bffbd7db91887607cfb5d5d0545f)), closes [#256](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/256) [#226](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/226) [#231](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/231)


### :sparkles: Novidades

* **icons:** atualiza forma de uso dos ícones do fontawesome ([2bb4da5](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/2bb4da5be0fd46e80908cd2cc392f64b25d36ddc))
* **footer:** logo opcional, click event e alinhamento de categorias ([7098bcd](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/7098bcd35ac272728575c53ffc40f7c82c0d336c)), closes [#238](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/238)
* **header:** search form e link para título ([cea6d8d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/cea6d8d880022357937934966a66a7d709e91e71)), closes [#237](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/237)


### :bug: Correções

* **message:** ajusta testes e estrutura do componente br-message ([5ac7be3](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/5ac7be3c5520d6924fdeaa121d87f84fbdb98326))
* **input:** corrige a digitação no input com e sem mascaras ([964873f](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/964873fcfb7c480484b5fdf05e1e85b6e1babca9))
* **breadcrumb:** corrige estilos ([f10385e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/f10385e6bf9c6aafa1f998071ef619565d68bb0e))
* **header:** corrige o css do campo de busca dentro do form ([7e31bf3](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/7e31bf327e9fd65cc9d106ecb4e9b96e144eddb3))
* **menu:** exclui classes que estavam deslocando os itens do menu contextual ([20281ce](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/20281ce508887104a042da3dbd1af5fc69369d6f))
* **input:** inclui exemplos storybook ([e1c6c0d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e1c6c0dead8c1b86f9fd8804a8b6e06ab4fa4f6a))
* **menu:** itens com ícones opcionais ([4a4957b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/4a4957b93c7aeef5e96d26cc4d9b8787ff80f19e)), closes [#239](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/239)
* tooltip das funcionalidades do header ([92da5b5](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/92da5b5765ca0a8936ee1b532d3d03b424094c8e))

## [1.4.2](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.4.1...v1.4.2) (28/12/2022)


### :memo: Documentação

* documentação para props json e eventos ([19886a6](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/19886a6c71d8bffbd7db91887607cfb5d5d0545f)), closes [#256](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/256) [#226](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/226) [#231](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/231)


### :bug: Correções

* **input:** inclui exemplos storybook ([e1c6c0d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e1c6c0dead8c1b86f9fd8804a8b6e06ab4fa4f6a))

## [1.4.1](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.4.0...v1.4.1) (09/12/2022)


### :bug: Correções

* **utils:** altera regex para tratar uso dos ":" apenas nas json props ([b7c1ffd](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b7c1ffd868c26b4d5f54db4f69ca335307d6268b))

## [1.4.1-alpha.1](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.4.0...v1.4.1-alpha.1) (31/10/2022)


### :bug: Correções

* corrige dep de dev causando loop ([c483121](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/c48312190d2044e8e66cada43bb102e7f55cbaad))

## [1.4.0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.3.1...v1.4.0) (2022-10-13)


### Features

* **notification:** adiciona o comportamento scrim ([78ed252](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/78ed252a727cd9c5796d2366f2869cf389ee9041))
* atualiza @govbr-ds/core para v3.1.0 ([5830157](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/5830157f1aaa377fa4144b5a9d8f910365e6d846))
* merge da main com a branch feat/br-input-masks ([049194e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/049194eebe3d0d200c708c3ef6af04798473c1ea))


### Bug Fixes

* ajusta testes do br-input com máscaras ([dc30624](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/dc30624286b6b2a5eb40b764c77cae520f623a72))
* atualiza pacotes de dependencias ([5ee4cd9](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/5ee4cd96b6c3b3ecba8eaeabe85e2f89422f4311))
* correcao no layout do notification ([76c34e5](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/76c34e57b5e2c6fb7ab7f4ab0a2398960f215dc8))
* remove css customizado do item ([fdc568e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/fdc568e871386946ed773fd38edcdb56a5622870))
* **menu/header:** retira obrigatoriedade de alguns ícones ([23a8668](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/23a86682840f7ea22419b855d77f6c5672446296))
* **notification:** retira scrim do componente notification ([e4dbd5b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e4dbd5bde076eedc8eee9e657c45eca736e55f45))

## [1.3.1](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.3.0...v1.3.1) (2022-09-20)


### Bug Fixes

* **breadcrumb:** cor, tamanho e posicionamento dos ícones ([e983d05](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e983d05ec10db4abb7891b180f1bfa7f82e44ade))
* **notification:** corrige quebras e alinha com o DS ([994925e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/994925edfd1d62dca23b8885998c1966c590133a))
* **header:** margem inferior conforme DS ([596ea7a](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/596ea7aae62ae88c4539463a0b35caade93057b0))

## [1.3.0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.2.0...v1.3.0) (2022-09-09)


### Features

* **menu:** lsita pode ser objeto e habilita click events ([bc7fb1c](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/bc7fb1c711f3a448c052681678ddd41a06eee837)), closes [#181](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/181) [#119](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/119) [#92](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/92)

## [1.2.0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.1.3...v1.2.0) (2022-09-05)


### Features

* **signin:** implementação do componente, testes e stories ([6d20f2b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/6d20f2be5ad09f53f09e0a52a4207fbb592c7bd5))
* **header:** possibilita a execução de funções nos links ([d1b2c4e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d1b2c4e5dbad2929cb4f1d996a2a1593701be4ea)), closes [#129](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/129)

## [1.1.3](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/compare/v1.1.2...v1.1.3) (2022-08-31)


### Bug Fixes

* **versão:** bump para versão 1.1.2 ([65bb3e2](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/65bb3e2528f69e7d0611e31b24ec1a60cf38d203))
* **deps:** define as dependências do react apenas para dev ([53ea367](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/53ea36724160ef4d1e0eee3f264cc84cb5cb6752))

## 1.1.2 (2022-08-29)

### Features

* card: implementacao barra de rolagem ([efce0a8](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/efce0a8))

### Bug Fixes

* versão: bump para versão 1.1.2 ([affc74a](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/affc74a))
* correção do comportamento do menu quando chamado por uma variável ([78dfc9b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/78dfc9b))
* correcao fundo escuro do componente br-tab ([3844436](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3844436))
* footer: corrige comportamento dos campos "redes sociais" e "parceiros" no modo mobile ([6a6b3a9](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/6a6b3a9))
* checkbox: estado indeterminado ([b8c745e](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/)), closes #1b8c745e50
* divider: remove css desnecessário no divider e implementa a utilização dos mixins no componente ([62c7993](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wb62c7993c/commit/))
* remove warnings do projeto ([3c30813](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3c30813))

## 1.0.0 (2022-07-12)


### ⚠ BREAKING CHANGES

* remove o tipo retangular do button e o notification

### Features

* [#15](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/15) br-notification ([b340520](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b34052062a12f77b089712a9ab02637b1f50cf47))
* [#88](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/88) corrigindo storybook ([3f2e62d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3f2e62d91b3d56c04a5621cfc839fd4556a53fef))
* adicao de comentario ([cc68fac](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/cc68facd8d63f85cf39c2c254d6f2bddcf98748d))
* adicao de comentario da propriedade isSticky ([a06a607](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a06a6077c1d9aff9abcd909c613c52a1751da75a))
* adicao de props no header action links e correcao nos testes unitarios ([d7a23a9](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d7a23a9e7f79efa99fde71a2386841b35baf8627))
* adiciona contadores ao tab ([dfa6a13](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/dfa6a130b63817257c7b7073578d149b7c1b103b))
* adiciona descricao para as propriedades do br-footer ([6d28537](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/6d28537a01116cc9c9c7b2bd88677be5656dbbd4))
* adiciona funcao para controlar tamanho e dimensionamento do br-menu no container ([d19a510](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d19a5105b76d86bb48096563e7cc7bc9863fc6e3))
* adiciona funcao para controlar tamanho e dimensionamento do br-menu no container ([3c08bfa](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3c08bfa65c89e0198d0f048bd525f752b768947e))
* adiciona pacote para download do reactjs ([c7c34b6](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/c7c34b646028d300001eedd4f95aaaf845731301))
* adiciona pacote para download do reactjs/angular ([b7a0053](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b7a00534f73c529dd6beb260ae39a90d44f8d580))
* adiciona propriedade counter para mostrar contadores ([dfe5291](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/dfe529151644e1a992fbcf358a719a51795246e6))
* adiciona propriedade para permitir ocultar o icone do br-menu ([3c5a2c6](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3c5a2c64b5ae6c5f8864017f49b385f3b00bd485))
* adiciona propriedade para permitir ocultar o icone do br-menu ([34be469](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/34be469d65698097ebb9ce96407f8d81dd219b71))
* adiciona propriedade para permitir ocultar o icone do br-menu ([3e2d5cf](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3e2d5cfd8610a1652f518f302b6c00fab96710eb))
* adiciona propriedade para permitir ocultar o icone do br-menu ([55218a2](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/55218a215b03aa8bc98a09dc389779ec3c4ca27a))
* adiciona propriedade para permitir ocultar o icone do br-menu ([314bdaa](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/314bdaa4389f2b4a690c2439cfd923910ca50013))
* adiciona propriedade que exibi descricao das propriedades dos sub componentes ([3cadf4a](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3cadf4adeb23315a5c44ac0ff10acd87c9f7f27e))
* adiciona propriedade que exibi descricao das propriedades dos sub componentes ([716c468](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/716c468a7a8b8ff3e5f74524c11120ebeafed4e7))
* adicionando govbr-ds-base.css ao build, gerando um dist/webcomponents.css ([71b2de0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/71b2de0db522227dfc246e9bbbd46c2f42e0b665))
* adicionando hook pos merge que faz o npm install ([be74bba](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/be74bba2b04fb1a4409f0838a4c71c4d8c516b41))
* adicionar exemplos ao storybook ([f6700a0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/f6700a0ca8a8139ad70bdf917eab23affc793dbf))
* **footer:** ajusta o footer para utilizar o br-list ([a31034d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a31034d61fe505664d2ec487c55ec94bcd1c9b9d))
* ajusta problemas de compatibilidade com a nova versão do govbr-ds ([d49d8ec](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d49d8ec63c5f7efcdec85f76b5b64b1437313767))
* ajusta storybook do br-menu e adiciona novas propriedades ao modelo do componente ([314b5ed](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/314b5ed00d63e30d2bc3fa0cba35be5bbba7122b))
* ajusta storybook do br-menu e adiciona novas propriedades ao modelo do componente ([0119897](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/01198971d5eefd30cf3fb91ec926894980e80517))
* ajuste no comentario da prop inverted ([e639567](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e639567951497b6adfa136525016d91563882aa1))
* ajustes no exemplo br-header.html ([91d73d1](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/91d73d1b8265e710d364b619a4c709589893ec8f))
* ajustes no storybook para a prop isStick ([b963970](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b9639700337a6fbc186f8f186d2923398a2be420))
* **tab:** aplica propriedade inverted a div principal do br-tab ([e0a0abf](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e0a0abfda17a6bbe2be467c87dbf48ebe8d15520))
* atualizando dependencias, devidos ajustes e removendo todos os erros e warnings do lint ([00953b8](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/00953b83dca0eabcd53c3af272d10db091f666f1))
* commit inicial da versão 1.0.0-alpha.0 ([bc0ee43](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/bc0ee436a0b7ae5d150399146e87b39477cf2eca))
* conversao das props antagonicas em uma unica prop ([c41d1c7](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/c41d1c7a192f25a7f023e2004ee95b1f88aea8d7))
* correcao dos links do header no storybook ([f5621c7](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/f5621c79bf006a73500b8ad1f4e26d391520a218))
* corrige execucao do pipeline do storybook ([05583eb](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/05583eb454dfcff8b3bb0a1c80e40203083f1062))
* corrige execucao do pipeline do storybook ([21ad157](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/21ad1577c9c44a24afa424b707f7e48e5f922416))
* corrige execucao do pipeline do storybook ([b9616e5](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b9616e56780185447b91949a1e0e24be8f03154c))
* corrigir breadcrumb na public ([0bd8190](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/0bd81900c853725c6cdfdc292ce46ffc9ddb31b7))
* Implementa responsividade do breadcrumb ([d0272c8](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d0272c8570839aa28028c71805442b3dd0a1a301))
* implementacao do sticky header ([844e24b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/844e24bfde593221b60754ffb7590e20a37f534c))
* inclui opcoes de largura e tracejado no divider ([2f831bd](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/2f831bd3a85f25e1bc15ee3cc5abe5b8c361d46a))
* merge com a develop ([f16ffd0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/f16ffd0e4b596fac06ef1221bd905b5d6721de80))
* merge from main ([5187dd0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/5187dd093f017fae7f35cb0717868c9126fc2aad))
* merging develop ([01fd2a2](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/01fd2a22c13bd3f2948646601d5bf0f583e194ab))
* modificacoes para passar e recuperar valor do br-input no evento de change ([7c466b6](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/7c466b6f01eac7b8570a7efc9f864c5612552353))
* mudando nome dos artefatos gerados na pasta dist para br-webcomponents.* ([448459c](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/448459c229c4d5b10bc9cab4bb9246b3d11220e9))
* remocao dos exemploes com imagem passada por slot ([45d3ae7](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/45d3ae79f870745a168f375f0eeff9b695e6b9a7))
* remove click do breadcrumb e ajusta exemplos ([a5def6f](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a5def6fabfb4da358d3c2fd7f87e1739c5566436))
* remove links desnecessarios no json do br-menu ([9562fef](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/9562fefb5febcfc836f62448fb338f3c1c9c6b93))
* Remove os subcomponentes do header que deixaram de serem usados devido a evolucao natural do mesmo. ([401b6e9](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/401b6e95735f755fdd4e8889d244aadae25b3fcc))
* renomeia o componente br-card ([c2c4530](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/c2c4530aabc2560faab12e61f58ae580643093d1))
* renomeia o nome do componente br-footer no storybook ([14da318](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/14da318c8da278031b24fb81f4fb73f3bc1df8aa))
* renomeia o nome do componente br-tab no storybook e no componente ([b3eb354](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b3eb3544fb90df9cba382b9671fb61698e70a592))
* renomeia o nome do pacote dos web componentes para @govbr/webcomponents ([c4acc77](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/c4acc772fc6bd8e0e4dbe83fa0301c17fae568ae))
* torna o icone do do componente br-mensagem opcional ([e8a9a87](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e8a9a873663f5e3164132afcbdb0b538f7d64530))
* variação contextual (menu) e botão rectangular (button) ([45e2cdd](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/45e2cdd1eff198117679d35cfebc6d990c090e07))


### Bug Fixes

* [#103](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/103) ([25346db](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/25346db0ce0df63ed295cc52e35485f97f5bc156))
* [#15](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/15) corrigindo notification ([b00ad54](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b00ad5498a0d3c6d1986068f0d220437b15cc571))
* [#65](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/65) retira controle para o slot header no story do br-card ([ec3bb5c](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/ec3bb5c6e06bbe64bffe8eac9528c8d3c568d718))
* adicao de css ([d64869a](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d64869abdf896c5dc396dfe8fae966d1c5b9b0a3))
* adiciona código html para o divider vertical funcionar na lista ([d4b321b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d4b321b60231fc9fee015f32b0e8c9163b6ed07e))
* adiciona dependencia para fontawesome ([70e5da3](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/70e5da328f4bf55f67c494e30ccb25edfb49704f))
* adicionando @vue/vue3-jest e devido ajuste ([3b77fee](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3b77fee1494787f6e1b84a6d27dc0add9e580d47))
* adicionando controle da prop inverted dos subcomponentes filhos do footer para o pai footer ([0709f63](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/0709f6312f709e0abe2c495965c3a891e3fbedc6))
* adicionando de volta a dependencia branch-name-lint ([4318c56](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/4318c56225f1291a7e2c32fdb9a6d8f7e6fd77bb))
* adicionando package-lock.json ([44459be](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/44459bea44989b5f1b3b2b9b803f60f3126bb4f8))
* Ajusta a propriedade checked do checkbox, facilitando saber se esta marcado ou não. ([a125b0b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a125b0b52c7ceb6bb95b00e8b76365318151630a))
* ajusta carga do conteudo do slot default ([84be9a5](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/84be9a55b0d213aacb1443904978bbb504a7f9d4))
* ajusta exemplo do br-avatar ([16adde2](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/16adde2fdfb397c2295e566b287682a3cc2911bf))
* ajusta exemplo do br-button ([90a4dd7](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/90a4dd79a87a462c042af1a975bf17ac6dc15b55))
* Ajusta problema de funcionamento do toggle no storybook. ([26d3c5c](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/26d3c5c63ae591a3b2b2762e12163b5c27ba349c))
* ajusta showicon e div extra no storybook ([d852ed5](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d852ed5f82013b36d8ba77e0e08a9c91420ec36b))
* ajusta storybook para carregar conteudo do slot default ([1d84575](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/1d845755c725d5334039ec12bb517dae8a9fe90a))
* **all:** ajusta testes jest dos webcomponents ([b52146a](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b52146adca3549d5f3b12227f7f9ce2e3d02077a))
* **menu:** Ajustar problema de classes do menu ([e389b36](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e389b367bef884918102854e90777b361267eb7b))
* alinhando codigo ([fefdd51](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/fefdd516c0d74170b6d8242e72c568d5efe07adb))
* Altera nome Breacrumb-crumb para crumb ([be37151](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/be371510ce79f2588c68447121f6054317f6fc82))
* alteracao da descricao do name no storybook ([f51e1f4](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/f51e1f4c49e0b922866efd68f2d2683c057f66f0))
* alterando imagem docker para node:16-bullseye ([9b1d97d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/9b1d97d8e06cfd9dbf552225660583ffccdf5f39))
* atualizando o .gitlab-ci.yml ([30b34dd](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/30b34dd96aef422152aa537e6bf701c755fb2ed6))
* atualizando versao do storybook ([ec00cfc](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/ec00cfcbad9478b8f199a868027668ac5d58c75e))
* atualuizando .gitlab-ci.yml ([4ac5d57](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/4ac5d575ae52d33934d08c5868cfb6bc79238d70))
* br-loading ([3f52bc2](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3f52bc2c7065250abb9b823000b46a6bac516fa7))
* correcao do br-tab no modo invertido ([efa0e96](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/efa0e969041a3727b1f5693777e71c9e59f4de0a))
* correção do menu push ([a8ed5fa](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a8ed5fa95ab34a02dc9bff6c24c96bb7188d9a81))
* correcao largura itens na lista data-toggle ([adbb236](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/adbb2362960481217660982715430caebee59e1b))
* correcao na densidade,hover do list ([81a51bb](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/81a51bb240609c8e65a0105bd9b2d92015a2da3d))
* corrige a execuçã do pipeline ([8e2a55a](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/8e2a55af53b158b3a71e444b4644b828b55cacb0))
* **header:** corrige densidade e tamanho da logo ([b125bf8](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/b125bf8c0fa77e9db0d7ee0f22d7ded52e66dc76))
* corrige menu de collapse do br-menu ([6827755](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/68277553acf1042dad8317dac37e46e7fd3a502f))
* corrige o problema de licenças duplicadas ([747abac](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/747abacab2d76ccb7bf0c92dbab4bb38b5ff897f))
* corrige problema identificado no checkbox event ([64ec39b](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/64ec39b5669ee1137c48dd53321621d01a485ad5))
* **jest:** corrige warnings do vue e jest ([1b01cdb](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/1b01cdb99e703f03ce6accd8dfb413001378b8b4))
* corrigindo click no item ([8e3aea5](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/8e3aea538b847867293d5135a3e84817c26381f2))
* **storybook:** corrigindo cor da fonte no show code do storybook ([d744d75](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d744d7568be432df6b9bd9c24ab39bc067ed64cd))
* corrigindo engines do package.json ([86a67fe](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/86a67fee0d7f74534a4b1034518a5c3f78555aa5))
* corrigindo estilo do show code do storybook ([037c60d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/037c60d99b424bfde2ddd9385adb7130d641cd5b))
* corrigindo testes do footer e seus subcomponentes ([381a99c](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/381a99cf4d9d6ebd78518820b95d5218a1b359df))
* corrigir posicionamento do avatar quando usado dentro do br-card ([67464b6](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/67464b6440be86063885a6b5b2e3687f99c37295))
* corrigir posicionamento do avatar quando usado dentro do br-card ([6f22b36](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/6f22b365b3d72c37c8ab508c74e66952a5ac66d2))
* DS - Atualiza as referências para o DS ([4822aab](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/4822aab47f4126c276d138f35da93047ace8fc41))
* **checkbox:** fix [#6](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/6) corrige checkbox indeterminate ([473aea8](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/473aea8cf62b746bebcd42d4a7f1c3193ee598ec))
* **header:** inclusao de exemplos de densidades ([798e173](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/798e173d2a3215b21ec5491628e6eeab83a5142f))
* iniciando bug do checkbox indeterminate ([ddf8f66](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/ddf8f66fd9aa3bbe5ad31f0bf742b7bcac45573d))
* largura dos itens no br-list horizontal ([923a0ff](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/923a0ff3dbb39f308aa7926b74a1b7f17a59fa26))
* melhorando exemplos de uso e storybook ([0ce8060](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/0ce8060b330eca3cf0003bf5db61917a0bf33d2d))
* merge com a main ([a83f8f0](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/a83f8f06c2ac8d3553e6503f01850c6f28a7a42f))
* merging main with fix/list-horizontal ([77de253](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/77de2539b0e991e00038b0890bbdad3eac5d95e2))
* **checkbox:** muda a prop. checked para saber se esta marcado ou não ([5f5a9ab](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/5f5a9ab5e35c2ef4f3d3d118064ddbcc7cd0dae9))
* removao da prop inverted no tab ([d07fd9d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d07fd9d7280f0d3d5851d56ebce41693616d6698))
* remove a etapa semgrep-sast da execução do pipeline ([3116dd1](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3116dd159da3b1a8499d27f0a7f08ce8d61f6dd1))
* remove a propriedade inverted tambem dos parametros de controle da historia do componente ([dc33597](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/dc335977a562da6fb8da69b2fede016073f0925a))
* remove exemplo com texto e icone do magic button ([6247083](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/6247083586b52f380e080cfc213c29a74d1d526a))
* remove exemplo com texto e icone do magic button ([d715a6f](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/d715a6f3d59e4f89ea17ae033613ab571d2106fb))
* remove licença duplicada ([db7b836](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/db7b83662045ee4e4175b805038962754bfedfc7))
* **input:** remove o valor vazio do atributos status ([5fbece5](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/5fbece511a5739a98ea6edaae33e712a9776e811)), closes [#13](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/issues/13)
* remove os passos do pipeline que estão impedindo os merge requests de serem executados ([3b520e8](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/3b520e85b506cfdb274b3c43e36a80d0092f6645))
* remove os passos do pipeline que estão impedindo os merge requests de serem executados ([724dfa8](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/724dfa82bafbb01ffb9c514e2ee6107c963b046e))
* remove os passos do pipeline que estão quebrando os merge requests ([bfcf57d](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/bfcf57d0315b8d1c6eb257f995575df90dba56b6))
* remove texto de licenças duplicados ([e0245c3](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/e0245c359b23464c36b35fab386a3ec95c788acf))
* removendo tabNavFontSize do template ([af25135](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/af25135c654b3801d538ddd3737857fd910875ea))
* retira depencia que precisa de arquivos de configuração ([1f38dc7](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/1f38dc7758ee0fd51d941c6df6342ba9a043ce0d))
* storybook agora inicia sem problemas e remove elementos ([7c6cc14](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/7c6cc1403c8e2dd0b7b520443acbf9edc87ec7ef))
* substituindo npm install por npm ci no .gitlab-ci.yml ([2578448](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/25784489ee5a4601ac55be8e339746e417450255))
* tab ([c2122c6](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/commit/c2122c6260e27edf55316e0f8c23fe28bb27d4f8))
