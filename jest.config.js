module.exports = async () => {
  return {
    collectCoverage: true,
    collectCoverageFrom: ['**/*.vue', '!**/node_modules/**'],
    coverageDirectory: 'coverage',
    coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
    moduleFileExtensions: ['js', 'ts', 'json', 'vue'],
    preset: '@vue/cli-plugin-unit-jest',
    testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],
    transform: {
      '.*\\.(vue)$': '@vue/vue3-jest',
      '.*\\.(js)$': 'babel-jest',
    },
    transformIgnorePatterns: ['/node_modules/(?!@govbr)'],
    testEnvironmentOptions: {
      customExportConditions: ['node', 'node-addons'],
    },
  }
}
